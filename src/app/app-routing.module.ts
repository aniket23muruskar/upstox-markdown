import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LiveStockComponent } from './live-stock/live-stock.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'live-stock', component: LiveStockComponent },
  { path: 'dashboard', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
