import { Component, OnInit } from '@angular/core';
import * as Highcharts from "highcharts/highstock";
import * as _ from 'lodash';
import { Options } from "highcharts/highstock";

import IndicatorsCore from "highcharts/indicators/indicators";
IndicatorsCore(Highcharts);

import { AppService } from '../app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  dataList = [];
  volumneList = [];
  loading = false;
  chartOptions: Options;

  constructor(
    private appService: AppService
  ) {
    
  }

  ngOnInit(): void {
    this.appService.getHistoricalList()
      .subscribe(response => {
        this.setChartOptions(response);
      });
  }

  async setChartOptions(response: any) {

    this.loading = true;

    this.chartOptions = {
      chart: {
        width: '1000',
        height: '500'
      }, 

      rangeSelector: {
        selected: 1
      },

      title: {
        text: 'Upstox Markdown'
      },

      yAxis: [
        {
          labels: {
            align: 'right',
            x: -3
          },
          title: {
            text: 'OHLC'
          },
          height: '60%',
          lineWidth: 2,
          resize: {
            enabled: true
          }
        },
        {
          labels: {
            align: 'right',
            x: -3
          },
          title: {
            text: 'Volume'
          },
          top: '65%',
          height: '35%',
          offset: 0,
          lineWidth: 2
        }
      ],

      plotOptions: {
        ohlc: {
            negativeColor: 'red',
            upColor: 'green'
         }
      },

      series: [
        {
          type: "ohlc",
          name: 'Upstox Markdown',
          data: this.appService.dataList,
          yAxis: 0,
          dataGrouping: {
            units: [[
                'week', // unit name
                [1] // allowed multiples
            ], [
                'month',
                [1, 2, 3, 4, 6]
            ]]
          }
        },
        {
          type: 'column',
          name: 'Volume',
          data: this.appService.volumneList,
          yAxis: 1
        }
      ]
    };
  }

  async parseResponse(response: any) {
    _.forEach(response, (value, key) => {
      let item = value.split(',');

      this.dataList.push([
        parseInt(item[0]),
        parseFloat(item[1]),
        parseFloat(item[2]),
        parseFloat(item[3]),
        parseFloat(item[4])
      ]);

      this.volumneList.push([
        parseInt(item[0]),
        parseInt(item[5])
      ]);
    });

    this.dataList.sort((a, b) => {
      if(a[0] === b[0]) {
        return 0
      }
      return (a[0] > b[0]) ? 1 : -1;
    });

    this.volumneList.sort((a, b) => {
      if(a[0] === b[0]) {
        return 0
      }
      return (a[0] > b[0]) ? 1 : -1;
    });
  }
}
