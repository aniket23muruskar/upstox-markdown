import { AppService } from './app.service'
import { Observable } from 'rxjs';

export function AppProviderFactory(provider: AppService) {
    return async () => await provider.getHistoricalList().subscribe();
}