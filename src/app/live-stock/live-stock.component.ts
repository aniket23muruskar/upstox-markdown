import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import * as Highcharts from "highcharts/highstock";
import * as _ from 'lodash';
import { Options } from "highcharts/highstock";

import IndicatorsCore from "highcharts/indicators/indicators";
import IndicatorZigzag from "highcharts/indicators/zigzag";
IndicatorsCore(Highcharts);

import { AppService } from '../app.service';
import { SocketioService } from '../socketio.service'

@Component({
  selector: 'app-live-stock',
  templateUrl: './live-stock.component.html',
  styleUrls: ['./live-stock.component.css']
})
export class LiveStockComponent implements OnInit, OnDestroy {
  Highcharts: typeof Highcharts = Highcharts;
  loading = false;
  dataList = [];
  volumneList = [];
  chartOptions: Options;
  subscription: Subscription


  constructor(
    private appService: AppService,
    private socketService: SocketioService
  ) {

    this.dataList = this.appService.dataList;
    this.volumneList = this.appService.volumneList;

    /*
    this.subscription = this.socketService.stock
      .subscribe(item => {
        this.setStockItem(item);
      })
    */
  }

  ngOnInit(): void {
    this.setChartOptions();
  }

  async setChartOptions() {
    this.loading = true;

    this.chartOptions = {
      chart: {
        width: '1000',
        height: '500'
      }, 

      rangeSelector: {
        selected: 1
      },

      title: {
        text: 'Upstox Markdown'
      },

      yAxis: [
        {
          labels: {
            align: 'right',
            x: -3
          },
          title: {
            text: 'OHLC'
          },
          height: '60%',
          lineWidth: 2,
          resize: {
            enabled: true
          }
        },
        {
          labels: {
            align: 'right',
            x: -3
          },
          title: {
            text: 'Volume'
          },
          top: '65%',
          height: '35%',
          offset: 0,
          lineWidth: 2
        }
      ],

      plotOptions: {
        ohlc: {
            negativeColor: 'red',
            upColor: 'green'
         }
      },

      series: [
        {
          type: "ohlc",
          name: 'Upstox Markdown',
          data: this.dataList,
          yAxis: 0,
          dataGrouping: {
            units: [[
                'week', // unit name
                [1] // allowed multiples
            ], [
                'month',
                [1, 2, 3, 4, 6]
            ]]
          }
        },
        {
          type: 'column',
          name: 'Volume',
          data: this.volumneList,
          yAxis: 1
        }
      ]
    };
  }

  setStockItem(item) {
    console.log(`setStockItem...`);
    let stockItem = item.split(',');
    console.log(stockItem);

    let OHLCItem = [
      parseInt(stockItem[0]),
      parseFloat(stockItem[1]),
      parseFloat(stockItem[2]),
      parseFloat(stockItem[3]),
      parseFloat(stockItem[4])
    ];

    console.log(OHLCItem);

    let volumeItem = [
      parseInt(stockItem[0]),
      parseInt(stockItem[5])
    ];

    this.dataList = [...this.dataList, OHLCItem];
    this.volumneList = [...this.volumneList, volumeItem];
  }

  ngOnDestroy() {
    //this.subscription.unsubscribe()
    //this.socketService.unsubscribe();
  }
}
