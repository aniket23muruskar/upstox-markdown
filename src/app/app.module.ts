import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HighchartsChartModule } from "highcharts-angular";

import { AppService } from './app.service';
import { AppProviderFactory } from './app.provider.factory';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LiveStockComponent } from './live-stock/live-stock.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    LiveStockComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HighchartsChartModule,
    AppRoutingModule
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
