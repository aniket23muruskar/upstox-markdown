import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { map, tap, delay } from 'rxjs/operators';
import * as _ from 'lodash';


@Injectable({
  providedIn: 'root'
})
export class AppService {
  httpOptions = {
    headers: new HttpHeaders(
      { 'Content-Type': 'application/json', 'Accept': 'application/json' },
    )
  };

  dataList = [];
  volumneList = [];

  constructor(
    private http: HttpClient,
  ) { }

  getHistoricalList() {
    return this.http.get<any>(`http://kaboom.rksv.net/api/historical?interval=8`, this.httpOptions)
      .pipe(
        map((response: any) => {
          this.parseResponse(response);
          return response;
        })
      );
  }

  async parseResponse(response: any) {
    _.forEach(response, (value, key) => {
      let item = value.split(',');

      this.dataList.push([
        parseInt(item[0]),
        parseFloat(item[1]),
        parseFloat(item[2]),
        parseFloat(item[3]),
        parseFloat(item[4])
      ]);

      this.volumneList.push([
        parseInt(item[0]),
        parseInt(item[5])
      ]);
    });

    this.dataList.sort((a, b) => {
      if(a[0] === b[0]) {
        return 0
      }
      return (a[0] > b[0]) ? 1 : -1;
    });

    this.volumneList.sort((a, b) => {
      if(a[0] === b[0]) {
        return 0
      }
      return (a[0] > b[0]) ? 1 : -1;
    });

    return {
      'dataList': this.dataList,
      'volumneList': this.volumneList
    };
  }
}
