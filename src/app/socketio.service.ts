import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { BehaviorSubject, Observable, of, throwError, Subject } from 'rxjs';
import { catchError, map, tap, delay } from 'rxjs/operators';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {
  socket;
  timeout;
  private stockItem = new Subject<any>();
  readonly stock = this.stockItem.asObservable();

  constructor() {
    //this.socket = io(`${environment.SOCKET_ENDPOINT}/watch`);
    //this.listenSocketEvents();
  }

  listenSocketEvents() {

    this.socket.on('connect', () => {
      this.socket.emit('sub', { state: true });
    });
    
    this.socket.on('disconnect', () => {
      console.log(this.socket.connected); // false
    });

    this.socket.on('data', (data: string, ack: any) => {
      console.log('response:' + data);
      this.stockItem.next(data);
      this.timeout = setInterval(() => {
        ack(1);
        console.log(`acknowledging..`);
      }, 1000);
    });

    this.socket.on('error', (data: string) => {
      console.log('error:' + data);
      this.stockItem.next(null);
    });
    
  }

  unsubscribe() {
    console.log(`unsubscribe socket...`);
    this.socket.emit('unsub', { state: false });
    clearInterval(this.timeout);
  }
}
